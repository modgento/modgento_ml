<?php

namespace Modgento\ML\Controller\Index;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Result\PageFactory;
use Phpml\Classification\SVC;
use Phpml\SupportVectorMachine\Kernel;
use Phpml\Classification\NaiveBayes;

class Index extends Action\Action
{

    protected $svc;
    protected $pageFactory;
    protected $productCollection;
    protected $categoryFactory;
    protected $directoryList;

    public function __construct(
        SVC $svc,
        PageFactory $pageFactory,
        DirectoryList $directoryList,
        Collection $productCollection,
        CategoryFactory $categoryFactory,
        Action\Context $context
    ) {
        $this->svc = $svc;
        $this->pageFactory = $pageFactory;
        $this->directoryList = $directoryList;
        $this->productCollection = $productCollection;
        $this->categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->pageFactory->create();
        $products = $this->productCollection;

        $classifier = new NaiveBayes();
        $path = $this->directoryList->getRoot() . "/var/";

        $samples = [];
        $labels = [];
        foreach ($products as $item) {
            $validIds = $this->filterDefault($item->getCategoryIds());
            foreach ($validIds as $id) {
                $samples[] = [(string) $item->getName()];
                $labels[] = (int) $id[0];
            }
        }
        $classifier->train($samples, $labels);
        $predict = ['Training Bag'];

        // $result should suggest category
        $result = $classifier->predict($predict);
        var_dump($result);
        $category = $this->categoryFactory->create()->load($result);
        var_dump($category->getName());
        return $page;
        /*
        The way I've done this, I grab every product and add it once under each label. large amount of label duplication and a poor result.
        Instead I should grab each category, find each product name in said category and add to array, adding the category id as a single label.

        However this is the first time I've got it to work at all
        */
    }

    public function filterDefault(array $categories)
    {
        $filtered = [];
        foreach ($categories as $category) {
            if ($category != 2) {
                $filtered[] = [$category];
            }
        }
        return $filtered;
    }
}
